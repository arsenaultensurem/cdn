/*$(window).scroll(function() {
var navbar = $('header');
var navheight = $('header').height();
	
 if ($(window).scrollTop() >= navheight ){
    navbar.addClass("sticky");
	$(document.body).css("margin-top", navheight);
  } else {
    navbar.removeClass("sticky");
	$(document.body).css("margin-top", "0");
  }

	});*/

var $plandetails = $('#results');
$plandetails.on('show.bs.collapse','.collapse', function() {
$plandetails.find('.collapse.show').collapse('hide');
});


$(function() {
    
    var body = $('body');
    var navbar = $('#sidebar');
    var navbarCollapse = $('.navbar-collapse');



    // Add the needed HTML elements for the plugin to work. 
    // All the elements are styled in navbar-sidemnu.css.
    
    body.append('<div class="side-menu-overlay"></div>');
    var overlay = $('.side-menu-overlay');

    body.append('<div class="side-menu"></div>');
    var sideMenu = $('.side-menu');

    sideMenu.append('<button class="close"><span aria-hidden="true">×</span></button>');
    var sideMenuCloseBtn = sideMenu.find('.close');

    sideMenu.append('<div class="contents"></div>');
    var sideMenuContents = sideMenu.find('.contents');



    // Configure Slide menu direction
    if(navbar.hasClass('navbar')) {
        sideMenu.addClass('side-menu-left');
    }


    // This event is trigerred when the user clicks the navbar toggle button.

    navbarCollapse.on('show.bs.collapse', function (e) {
        // Stop the default navbar behaviour (don't open the collapse navigation).
        e.preventDefault();

        // Instead we move the navbar contents and add them to our side menu.
        $('#sidemenuwrapper').appendTo('.side-menu .contents');
        
        // Animate the side menu into frame.
        slideIn();
    });


    // Hide the menu when the "x" button is clicked.
    
    sideMenuCloseBtn.on('click', function(e) {
        e.preventDefault();
        slideOut();
			
    });

    // Hide the menu when the overlay element is clicked.
    
    overlay.on('click', function() {
	
        slideOut();
	
    });
	
    // Listen for changes in the viewport size.
    // If the original navbar collapse is visible then the nav is expanded.
    // Hide/Show the menu accordingly.
    
    $(window).resize(function(){
        if(!navbarCollapse.is(":visible") && body.hasClass('side-menu-visible')) {
            sideMenu.show();
            overlay.show();
			slideIn()
        }
        else {
            sideMenu.hide();
            overlay.hide();
			 slideOut();
        }
    });
    
    function slideIn() {
        body.addClass('overflow-hidden');
        sideMenu.show();
        setTimeout(function() {    
            body.addClass('side-menu-visible');
            overlay.show();
        }, 50);
    }
    
    function slideOut() {
        body.removeClass('side-menu-visible');
        overlay.hide();
        setTimeout(function() {
            sideMenu.hide();
            body.removeClass('overflow-hidden');
        }, 400);
					$('#sidemenuwrapper').appendTo('#desktopmenu');
    }
	
});