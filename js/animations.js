$(".hero h1, .apply__success h2, .disclaimer__wrapper, .apply__fail h2").attr({
    "data-aos": "fade-up",
    "data-aos-duration": "1000"
});
$(".quote__form, .apply__form, .disclaimer__form, .results__group, .comparison, .apply__success, .apply__fail").attr({
    "data-aos": "fade-up",
    "data-aos-duration": "900"
});
$(".form__submit-wrapper, .hero__button-wrapper, .comparebar__button-wrapper, .results__plan-premium, .comparison__apply-wrapper, .apply__success-section .icon, .apply__fail-section .icon").attr({
    "data-aos": "zoom-in",
    "data-aos-duration": "300",
	"data-aos-delay": "300"
});
$(".hero p").attr({
    "data-aos": "fade-up",
    "data-aos-duration": "800"
});
$(".results__plan-logo, .aside__plan-logo, .comparison__plan-logo").attr({
    "data-aos": "zoom-in",
    "data-aos-duration": "600",
});

$(".aside").attr({
    "data-aos": "fade",
	"data-aos-delay": "400",
    "data-aos-duration": "1000"
});
AOS.init();

AOS.init({
    offset: 0,
    duration: 1000,
    easing: 'ease-out-back',
    delay: 50,
	once: true,
	disable: 'mobile'
});

